package com.mypages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage {

    //locator
    private By hHeader = By.xpath("//span[contains(.,'My account')]");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getHeader() {
        return getElement(hHeader);
    }

    //page actions
    public String getHomePageTitle()
    {
        return getPageTitle();
    }

    public String getHomePageHeader()
    {
        return getPageHeader(hHeader);
    }
}
