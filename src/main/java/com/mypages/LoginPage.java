package com.mypages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage extends BasePage {

    //page locators
    private By signInLink = By.xpath("//a[contains(text(),'Sign in')]");
    private By email = By.id("email");
    private By password = By.id("passwd");
    private By loginBtn = By.cssSelector("#SubmitLogin > span");
    private By header = By.cssSelector(".blockbestsellers:nth-child(1)");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    //public getters
    public WebElement getSignInLink() {
        return getElement(signInLink);
    }

    public WebElement getEmail() {
        return getElement(email);
    }

    public WebElement getPassword() {
        return getElement(password);
    }

    public WebElement getLoginBtn() {
        return getElement(loginBtn);
    }

    public WebElement getHeader() {
        return getElement(header);
    }

    public String getLoginPageTitle()
    {
       return getPageTitle();
    }

    public String getLoginPageHeader()
    {
        return getPageHeader(header);
    }

    public HomePage doLogin(String username, String password)
    {
        getSignInLink().click();
        getEmail().sendKeys(username);
        getPassword().sendKeys(password);
        getLoginBtn().click();

        return getInstance(HomePage.class);
    }

    //Method overloading
    public void doLogin()
    {
        getSignInLink().click();
        getEmail().sendKeys("");
        getPassword().sendKeys("");
        getLoginBtn().click();
    }
}
