package com.myTests;

import com.mypages.HomePage;
import com.mypages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest{

    @Test(priority = 1)
    public void verifyLoginPageTitleTest()
    {
       String title = page.getInstance(LoginPage.class).getLoginPageTitle();
        System.out.println("Title = "+ title);
        Assert.assertEquals(title, "My Store");
        }

    @Test(priority = 2)
    public void verifyLoginPageHeaderTest()
    {
        String header = page.getInstance(LoginPage.class).getLoginPageHeader();
        System.out.println("Header = "+ header);
         Assert.assertEquals(header, "BEST SELLERS");
    }

    @Test(priority = 3)
    public void doLoginTest()
    {
      HomePage homePage = page.getInstance(LoginPage.class).doLogin("yogeshkolhe11@gmail.com", "Selenium123@");
      String headerHome = homePage.getHomePageHeader();
        System.out.println("Home Page Header = "+headerHome);
        Assert.assertEquals(headerHome, "My account");
    }
}
