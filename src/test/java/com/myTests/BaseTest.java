package com.myTests;

import com.mypages.BasePage;
import com.mypages.Page;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver;
    public Page page;

    @BeforeMethod
    public void setUpTests()
    {
            ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.setExperimentalOption("useAutomationExtension",false);
            System.setProperty("webdriver.chrome.driver", "src" + File.separator  + "main" + File.separator + "resources" + File.separator  + "Drivers" + File.separator + "chromedriver.exe");
            driver = new ChromeDriver(chromeOptions);
            driver.get("http://automationpractice.com/");
            driver.manage().window().maximize();
            driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

            page = new BasePage(driver);
    }

    @AfterMethod
    public void tearDown()
    {
        driver.quit();
    }

}
