Assessment Details :
You are required to review the below test case, and automate it accordingly using Selenium Webdriver. You can use either Java or C# to create the test framework.
 
Consider using best practises regarding test automation:
	· Data driven Tests (no hard coded values in the source code). Same code and tests, different values.
	· Re-usable code (use methods).
	· Maintainable Code
			o Readable code.
			o Naming conventions.
			o Coding standards.
	· Code comments to describe “What” is happening.
 
Additional BONUS points for:
	· Using PageObjects.
	· Having the source code maintained and versioned in a source code repository (and demonstrating your ability to use the repository effectively).
	· Being able to execute on different browser with minimum code tweaks.
	· Implementing the automation is a CI/CD pipeline
 
On the day of your demo, you will be expected to:
	· Explain the benefits of selecting the relevant tool (Selenium) [2 minutes]
	· Explain the shortcomings of selecting the relevant tool (Selenium) [2 minutes]
	· Describe how you designed the framework [2 minutes]
	· Describe a difficulty you encountered, and how you resolved it [2 minutes]
	· Execute the test [5 minutes]
 
URL - http://automationpractice.com/

Credentials :
UserName: yogeshkolhe11@gmail.com
Password: Selenium123@
 
Scenarios to automate :
	· Scenario 1: Validate the Page Title on the homepage
	· Scenario 2: Register a new user
	· Scenario 3 : Add random items to the cart with random quantities and checkout
	· Scenario 4 : View the order history and send an email with the order details (select a random order number)
			o Note- this should cater for 1 order or even if no orders exist.
	· Scenario 5 – Submit a query using the contact us page

	Note 1: Make sure that OOps concepts should be implemented.
	              1. Abstract class
	              2. Method overloading
	              3. Method overriding
	              4. Encapsulation : private/public
	              5. Java Generics
	              6. Wrapper Methods
	              7. Object creation
	              9. constructor
	              10. Inheritance
	              11. Interface
	              12. Exception Handling
 

